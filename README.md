# Reversible Jacket

![Jacket logo](./varsity-jacket.png)

## Overview

This is a demo project to show a practical pattern for developing a modern web frontend and backend.
The goal is to implement a web application using the [Backends For Frontend (BFF)](https://samnewman.io/patterns/architectural/bff/) pattern in a developer-friendly way.

During development, the frontend proxies the backend API.
In production, the roles are reversed and the backend hosts the frontend's static assets.
Hence, "Reversible Jacket."

Although you _could_ use this as a starter project, it's not recommended.
Things that would make this a real boilerplate, such as TypeScript and unit tests, are missing.
This demo is unlikely to be updated regularly and may have errors or security flaws.
**Use at your own risk.**
Instead, consider this as a proof of concept purposefully kept simple to explain the concept.
This pattern could easily be implemented with a wide variety of tools (Deno/Go/.NET/PHP/Python/whatever).
If you do so, please let me know (see below)!

## Principles

These principles are core attributes of the Reversible Jacket:

* It extends [Jamstack] and [12-factor] methodologies.
  The frontend and backend are two halves of an atomically deployed whole.
* It avoids [CORS] problems by keeping the final web app [single-origin].
* It isolates the developer from differences between development and production.
  Developers should be reasonably confident that the simulation is accurate.
* Developer productivity is paramount, and hot reload is an essential component.
* The application should always be runnable by default.
  Developers shouldn't have to change the application to run in different contexts.
  The same code should work regardless of how it was launched.
* User-facing page routes will be handled by the frontend.
  It should support web apps that use the [History API].

[Jamstack]: https://jamstack.org/
[12-factor]: https://12factor.net/
[CORS]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
[single-origin]: https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy
[History API]: https://developer.mozilla.org/en-US/docs/Web/API/History_API

## Usage

Using Docker Compose is recommended, but it is also possible to run the project with Node.js locally.
In either case, you can verify that the frontend and backend are running by opening your browser to the following:

* Frontend: <http://localhost:3000/>
* Backend: <http://localhost:3001/>

### Option 1: Docker Compose

You should be able to run [Docker Compose](https://docs.docker.com/compose/).
If you don't have it, the simplest option might be to install [Docker Desktop](https://docs.docker.com/get-docker/).

In a terminal window, change to the root directory of this project and run the command `docker compose up`.
Alternatively, the script [`scripts/run.sh`](./scripts/run.sh) will do the same when launched from any directory.

### Option 2: local Node.js

You should have [PNPM](https://pnpm.io/) and [Node.js](https://nodejs.org/) installed.

1. Open two terminal windows:
   * One should be in [`backend`](./backend/), as in `cd backend`.
   * One should be be in [`frontend`](./frontend/), as in `cd frontend`.
2. In both terminals, execute the following commands:

   ```sh
   pnpm install
   pnpm start
   ```

## How it works

During development, the frontend code is bundled and served by [Vite](https://vitejs.dev/).
Vite watches the code for changes and uses HMR to reload changes and refresh any open browser.
The server is also configured to restart on any code changes, but there's no browser refresh.
In addition, Vite is [configured](https://vitejs.dev/config/#server-proxy) to proxy any requests that begin with `/api` to `http://localhost:3001`.
When the stack is launched in Docker compose, the frontend server uses the proxy URL `http://backend:3001` instead.

When the root Dockerfile is built for production however, the roles are reversed.
The first part of the multi-stage build prepares the server and builds the static frontend files.
It then copies the server code and node modules to a distroless image.
It also puts the output of the frontend build in the "public" directory.
Files in the server's `/public` directory (such as index.html) are served from the server root URL and expire immediately.
Files in `/public/assets` directory however, are assumed to be hashed by Vite and are given long expiration headers.

## Technology

Here's the tech in use for this specific demo and links to more documentation:

* [Docker](https://docs.docker.com/get-started/overview/) - an open platform for developing, shipping, and running applications.
* [Node.js](https://nodejs.org/docs/) - a JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Nodemon](https://nodemon.io/) - a utility to restart the server.
* [PNPM](https://pnpm.io/) - a fast, disk space efficient package manager for Node.
* [sirv](https://github.com/lukeed/sirv) - an optimized middleware & CLI application for serving static files.
* [Vite](https://vitejs.dev/guide/) - a frontend dev server and bundler.

## Other Implementations

Here are some other implementations of the Reversible Jacket pattern:

* None yet!
  
If you'd like to have your project linked here, please submit a merge request to change [this document].

[this document]: https://gitlab.com/barnabas/reversible-jacket/-/blob/main/README.md

[Jacket icon created by Freepik - Flaticon](https://www.flaticon.com/free-icons/clothes)
