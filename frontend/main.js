import "./style.css";

let isFetching = false;

function updateDemo() {
  const demoEl = document.querySelector("#demo");
  if (isFetching || !demoEl) return;
  isFetching = true;
  demoEl.innerHTML += "\n..."; // fetching indicator

  fetch("/api/now")
    .then((res) => {
      if (!res.ok) throw new Exception(res.statusText);
      return res.json();
    })
    .then((json) => {
      demoEl.innerHTML = JSON.stringify(json, null, 2);
    })
    .catch((err) => {
      const msg = err ? err.message : "Unknown error.";
      demoEl.innerHTML = msg + " Is the backend server running?";
    })
    .finally(() => {
      isFetching = false;
    });
}

document.querySelector("#app").innerHTML = `
  <h1>Reversible Jacket Demo</h1>
  <pre id="demo"></pre>
`;

updateDemo();
let handle = setInterval(updateDemo, 5000);

// account for Vite HMR, see https://vitejs.dev/guide/api-hmr.html
if (import.meta.hot) {
  import.meta.hot.dispose(() => {
    clearInterval(handle);
  });
}
